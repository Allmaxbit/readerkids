// Initialize your app
'use strict';

var langs =
    [['Afrikaans',       ['af-ZA']],
        ['Bahasa Indonesia',['id-ID']],
        ['Bahasa Melayu',   ['ms-MY']],
        ['Català',          ['ca-ES']],
        ['Čeština',         ['cs-CZ']],
        ['Dansk',           ['da-DK']],
        ['Deutsch',         ['de-DE']],
        ['English',         ['en-AU', 'Australia'],
            ['en-CA', 'Canada'],
            ['en-IN', 'India'],
            ['en-NZ', 'New Zealand'],
            ['en-ZA', 'South Africa'],
            ['en-GB', 'United Kingdom'],
            ['en-US', 'United States']],
        ['Español',         ['es-AR', 'Argentina'],
            ['es-BO', 'Bolivia'],
            ['es-CL', 'Chile'],
            ['es-CO', 'Colombia'],
            ['es-CR', 'Costa Rica'],
            ['es-EC', 'Ecuador'],
            ['es-SV', 'El Salvador'],
            ['es-ES', 'España'],
            ['es-US', 'Estados Unidos'],
            ['es-GT', 'Guatemala'],
            ['es-HN', 'Honduras'],
            ['es-MX', 'México'],
            ['es-NI', 'Nicaragua'],
            ['es-PA', 'Panamá'],
            ['es-PY', 'Paraguay'],
            ['es-PE', 'Perú'],
            ['es-PR', 'Puerto Rico'],
            ['es-DO', 'República Dominicana'],
            ['es-UY', 'Uruguay'],
            ['es-VE', 'Venezuela']],
        ['Euskara',         ['eu-ES']],
        ['Filipino',        ['fil-PH']],
        ['Français',        ['fr-FR']],
        ['Galego',          ['gl-ES']],
        ['Hrvatski',        ['hr_HR']],
        ['IsiZulu',         ['zu-ZA']],
        ['Íslenska',        ['is-IS']],
        ['Italiano',        ['it-IT', 'Italia'],
            ['it-CH', 'Svizzera']],
        ['Lietuvių',        ['lt-LT']],
        ['Magyar',          ['hu-HU']],
        ['Nederlands',      ['nl-NL']],
        ['Norsk bokmål',    ['nb-NO']],
        ['Polski',          ['pl-PL']],
        ['Português',       ['pt-BR', 'Brasil'],
            ['pt-PT', 'Portugal']],
        ['Română',          ['ro-RO']],
        ['Slovenščina',     ['sl-SI']],
        ['Slovenčina',      ['sk-SK']],
        ['Suomi',           ['fi-FI']],
        ['Svenska',         ['sv-SE']],
        ['Tiếng Việt',      ['vi-VN']],
        ['Türkçe',          ['tr-TR']],
        ['Ελληνικά',        ['el-GR']],
        ['български',       ['bg-BG']],
        ['Pусский',         ['ru-RU']],
        ['Српски',          ['sr-RS']],
        ['Українська',      ['uk-UA']],
        ['한국어',            ['ko-KR']],
        ['中文',             ['cmn-Hans-CN', '普通话 (中国大陆)'],
            ['cmn-Hans-HK', '普通话 (香港)'],
            ['cmn-Hant-TW', '中文 (台灣)'],
            ['yue-Hant-HK', '粵語 (香港)']],
        ['日本語',           ['ja-JP']],
        ['हिन्दी',            ['hi-IN']],
        ['ภาษาไทย',         ['th-TH']]];
var select_language = document.getElementById("select_language");
var select_dialect = document.getElementById("select_dialect");
for (var i = 0; i < langs.length; i++) {

    select_language.options[i] = new Option(langs[i][0], i);
}
select_language.selectedIndex = 7;
updateCountry();
select_dialect.selectedIndex = 6;


function updateCountry() {
    for (var i = select_dialect.options.length - 1; i >= 0; i--) {
        select_dialect.remove(i);
    }
    var list = langs[select_language.selectedIndex];
    for (var i = 1; i < list.length; i++) {
        select_dialect.options.add(new Option(list[i][1], list[i][0]));
    }
    select_dialect.style.visibility = list[1].length == 1 ? 'hidden' : 'visible';
}


var recognition;
var recognizing = false;
var final_transcript = '';
var currentString = '';

var two_line = /\n\n/g;
var one_line = /\n/g;
function linebreak(s) {
    return s.replace(two_line, '<p></p>').replace(one_line, '<br>');
}
function spaceBreak(s) {
    var re = /\s* \s*/
    return s.charAt(0) == " " ? s.substr(1).split(re) : s.split(re);
}

function in_array(value, array) {
    for(var i = 0; i < array.length; i++)
    {
        if(array[i] == value) return true;
    }
    return false;
}

var first_char = /\S/;
function capitalize(s) {
    return s.replace(first_char, function(m) { return m.toUpperCase(); });
}
var annyangStart = function() {
    console.log('start');
    jQuery('#start').trigger('click');
};
var annyangStop = function() {
    console.log('stop');
    jQuery('#stop').trigger('click');
};
var annyangPause = function() {
    console.log('pause');
    jQuery('#pause').trigger('click');
};
var annyangNext = function() {
    console.log('next');
    jQuery('#next').trigger('click');
};


jQuery(document).ready(function() {
    //console.log('ready');




    //recognition begin;
    recognition = new webkitSpeechRecognition();
    recognition.continuous = true;
    recognition.interimResults = true;

    recognition.onresult = function(event) {

        var interim_transcript = '';
        if (typeof(event.results) == 'undefined') {
            recognition.onend = null;
            recognition.stop();
            return;
        }
        for (var i = event.resultIndex; i < event.results.length; ++i) {
            if (event.results[i].isFinal) {
                final_transcript += event.results[i][0].transcript;
                currentString = event.results[i][0].transcript;
                //console.log(currentString, typeof currentString);

                    if (in_array('start',spaceBreak(currentString))) {
                        annyangStart();
                    } else if (in_array('stop',spaceBreak(currentString))) {
                        annyangStop();
                    }else if (in_array('pause',spaceBreak(currentString))) {
                        annyangPause();
                    }else if (in_array('next',spaceBreak(currentString))) {
                        annyangNext();
                    }

            } else {
                interim_transcript += event.results[i][0].transcript;
            }
        }


        jQuery('#text-recognition').html(final_transcript);

        //console.log(linebreak(final_transcript));
        //console.log(linebreak(interim_transcript));
        //console.log(event);
    };
    recognition.onstart = function() {
        recognizing = true;
    };
    recognition.onend = function() {
        recognizing = false;
        currentString ='';
    };
    recognition.onerror = function(event) {
        if (event.error == 'no-speech') {
            console.log(event.error);
        }
        if (event.error == 'audio-capture') {
            console.log(event.error);
        }
        if (event.error == 'not-allowed') {
            console.log(event.error);
        }
    };
    //recognition end;


    var realComment = jQuery('#real-comment');
    var wrapComment = jQuery('#wrap-comment');
    var maxCaracther = 98;


    jQuery('#startRecognition').on('click', function() {
        if (recognizing) {
            recognition.stop();
            return;
        }
        recognition.lang = select_dialect.value;
        recognition.start();
    });
    jQuery('#stopRecognition').on('click', function() {
            recognition.stop();
    });

    jQuery('#start').on('click', function() {


        var textPointer = getText(document.getElementById('real-comment'));
        var valRealComment = realComment.val();
        if (!valRealComment) {
            return alert('Text is required!');
        }
        var check = false;
        valRealComment = valRealComment.split("\n").map(function(b, i) {
            if (!b) {
                return b;
            }
            var currentTextLine = b;
            try {
                var currentLength = 0;
                var currentLineText = [];
                var allLine = [];
                currentTextLine.split(' ').map(function(text, n) {
                    (function(text) {
                        currentLength += text.length + 1;
                        if (currentLength > maxCaracther) {
                            allLine.push(currentLineText.join(' '));
                            currentLength = text.length;
                            currentLineText = [];
                        }
                        currentLineText.push(text);
                    })(text);
                });
                allLine.push(currentLineText.join(' '));
                var val = allLine.map(function(text, n) {
                    console.log(text, n, 'text, n', textPointer, i);
                    if (
                        textPointer.currentLine == i && text.match(/[^ ]+/g).indexOf(textPointer.text) != -1 && textPointer.line == n
                    ) {
                        check = true;
                        return '<div class="black-comment">' + text + '</div>';
                    } else if (!check) {
                        return '<div class="gray-comment">' + text + '</div>';
                    } else {
                        return '<div class="white-comment">' + text + '</div>';
                    }
                });
                return val.join('');
            } catch (e) {
                if (
                    textPointer.currentLine == i && b.match(/\w+/g).indexOf(textPointer.text) != -1
                ) {
                    check = true;
                    return '<div class="black-comment">' + b + '</div>';
                } else if (!check) {
                    return '<div class="gray-comment">' + b + '</div>';
                } else {
                    return '<div class="white-comment">' + b + '</div>';
                }
            }
        });
        realComment.hide();
        wrapComment.html(valRealComment.join("\n")).show();
    });
    jQuery('#stop').on('click', function() {
        wrapComment.html('').hide();
        realComment.show();
    });
    jQuery('#next').on('click', function() {
        var currentText = jQuery('.black-comment');
        if (currentText.length == 0) {
            return alert('Finish text!');
        }
        var eq = jQuery('#wrap-comment > div').index(currentText);
        currentText.attr('class', 'gray-comment');
        var next = jQuery('#wrap-comment > div').eq(eq + 1);
        if (next.length == 0) {
            return alert('Finish text!');
        }
        next.attr('class', 'black-comment');
    });

    function getText(el) {
        var val = el.value;
        var position = el.selectionStart;
        var text = '';
        for (var i = position, max = val.length; i < max; i++) {
            var word = val[i];
            if (word == ' ' || word == '\n') {
                break;
            }
            text += word;
        }
        var textBefore = '';
        for (var i = 0, max = position; max > i; max--) {
            var word = val[max - 1];
            if (word == ' ' || word == '\n') {
                break;
            }
            textBefore += word;
        }
        var newTextBefore = '';
        for (var i = 0, max = textBefore.length; max > i; max--) {
            newTextBefore += textBefore[max - 1];
        }
        var currentText = newTextBefore + text;
        var textBefore = '';
        var check = false;
        for (var i = 0, max = position; max > i; max--) {
            var word = val[max - 1];
            if (!check && word != ' ') {
                check = true;
            } else if (!check) {
                continue;
            }
            if (word == '\n') {
                break;
            }
            textBefore += word;
        }
        var newTextBefore = '';
        for (var i = 0, max = textBefore.length; max > i; max--) {
            newTextBefore += textBefore[max - 1];
        }
        var line = 0;
        var currentTextLine = newTextBefore + currentText;
        try {
            var currentLength = 0;
            currentTextLine.split(' ').map(function(b, i) {
                currentLength += b.length + 1;
                console.log('currentLength', currentLength, 'b', b);
                if (currentLength > maxCaracther) {
                    line++;
                    currentLength = b.length;
                }
            });
        } catch (e) {
            console.log(e);
        }
        try {
            var currentLine = val.substr(0, position).match(/\n/g).length;
        } catch (e) {
            var currentLine = 0;
        }
        return {
            text: currentText,
            position: position,
            currentLine: currentLine,
            line: line
        };
    }

    //annyang begin;
    //if (annyang) {
    //    var commands = {
    //        'start': annyangStart,
    //        'stop': annyangStop,
    //        'pause': annyangPause,
    //        'next': annyangNext
    //    };
    //    // Debug mode
    //    annyang.debug();
    //    // Add our commands to annyang
    //    annyang.addCommands(commands);
    //    annyang.setLanguage('en');
    //    // Start listening. You can call this here, or attach this call to an event, button, etc.
    //    annyang.start();
    //}
    //annyang end;
});